
const set_token = (token) => {
    sessionStorage.setItem('token', token);
}
const get_token = () => {
   return sessionStorage.getItem('token');
}
const set_username = (username) =>{
    return localStorage.setItem('username', username);
}
const get_username = () =>{
    return localStorage.getItem('username');
}
const loginOut = () => {
    sessionStorage.clear();
    localStorage.clear();
}
export default {
    set_token,
    get_token,
    set_username,
    get_username,
    loginOut
}
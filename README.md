<h1 align="center">Welcome to fund-income-frontend 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D5.5.0-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D9.3.0-blue.svg" />
  <a href="none" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://github.com/kefranabg/readme-md-generator/graphs/commit-activity" target="_blank">
    <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" />
  </a>
  <a href="https://github.com/kefranabg/readme-md-generator/blob/master/LICENSE" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/github/license/MasterTang-T/fund-income-frontend" />
  </a>
</p>

> The front end of a financial income project

### 🏠 [Homepage](http://www.mastertang.top)

### ✨ [Demo](http://www.mastertang.top)

## Prerequisites

- npm >=5.5.0
- node >=9.3.0

## Install

```sh
npm install
```

## Usage

```sh
npm run start
```


## Author

👤 **MasterTang**

* Website: https://github.com/MasterTang-T
* Github: [@MasterTang-T](https://github.com/MasterTang-T)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/MasterTang-T/fund-income-frontend/issues). You can also take a look at the [contributing guide](none).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2021 [MasterTang](https://github.com/MasterTang-T).<br />
This project is [MIT](https://github.com/kefranabg/readme-md-generator/blob/master/LICENSE) licensed.

***